﻿import { Component, OnInit, ViewEncapsulation, Input, HostListener } from '@angular/core';
import { AudioService } from '../audio.service';
import { Audio } from '../audio.model';
import { PlayData } from '../play-data.model';
@Component({
    selector: 'audio-studio',
    styleUrls: ['audio-studio.component.css'],
    templateUrl: 'audio-studio.component.html',
})
export class AudioStudioComponent implements OnInit {

    public playList: Audio[];
    public playData: PlayData;
    public audios: Audio[];
    public playMode:any = [
        {
            id:0,
            pic: './assets/img/audio/1.png'
        },
        {
            id:1,
            pic: './assets/img/audio/8.png'
        },
        {
            id:2,
            pic: './assets/img/audio/10.png'
        }
    ]
    public selectedMode:number;
    public isRotate: boolean = false;
    public musicList:any = []

    constructor(public audio: AudioService) {
        this.musicList = [
            {
                Id: 1,
                Url: './assets/audio/1.mp3',
                Title: '星球坠落 (Live)',
                Cover: './assets/img/audio/1.jpg',

            },
            {
                Id: 2,
                Url: './assets/audio/2.mp3',
                Title: '背对背拥抱',
                Cover: './assets/img/audio/2.jpg',
            },
            {
                Id: 3,
                Url: './assets/audio/3.mp3',
                Title: '不将就',
                Cover: './assets/img/audio/3.jpg',
            },
            {
                Id: 4,
                Url: './assets/audio/Fantastic Baby.mp3',
                Title: 'Fantastic Baby',
                Cover: './assets/img/audio/4.jpg',
            },
            {
                Id: 5,
                Url: './assets/audio/I Know Places.mp3',
                Title: 'I Know Places',
                Cover: './assets/img/audio/5.jpg',
            }
        ]
        audio.AddArray(this.musicList);
    }

    public ngOnInit() {
        this.selectedMode = 0;
        if(this.musicList.length > 0){
            this.isRotate = true;
            this.playList = this.audio.PlayList();
            this.playData = this.audio.PlayData();
        }
    }
    // 跳转到指定播放时间
    public Skip(e) {
        this.audio.Skip(e.layerX /
        document.getElementById('audio-total').getBoundingClientRect().width);
    }
    // 改变播放模式
    changeMode(id){
        if(this.selectedMode == 2 ){
            this.selectedMode = 0;
        }else{
            this.selectedMode = id + 1;
        }
        this.audio.ChangeMode(this.selectedMode);
    }
    // 切换播放状态
    togglePlay(){
        if(this.playList){
            this.isRotate = !this.isRotate;
        }
        this.audio.Toggle();
    }
    // 播放下一首歌曲
    playNext(){
        this.isRotate = true;
        this.audio.Next('man');
    }
    // 播放上一首歌曲
    playPre(){
        this.isRotate = true;
        this.audio.Prev('man');
    }
}
