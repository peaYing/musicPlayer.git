﻿import { Injectable } from '@angular/core';
import { Audio } from './audio.model';
import { PlayData } from './play-data.model';

/**
 * 音频服务，只关心播放列表控制与进度控制
 * 不提供组件支持，只提供列表控制方法接口及进度控制接口
 */
@Injectable()
export class AudioService {
    // 主音频标签
    private _audio: HTMLAudioElement;
    // 当前列表中的音频
    private playList: Audio[];
    // 当前播放的数据
    private playData: PlayData;
    private listenInterval;
    public selectedMode = 1;
    /**
     * 创建新的音频标签
     */
    constructor() {
        this._audio = document.createElement('audio');
        this._audio.autoplay = false;
        this._audio.onplay = () => {
            let that = this;
            this.listenInterval = window.setInterval(() => {
                that.playData.Current = that._audio.currentTime;
                that.playData.Url = that._audio.src;
                that.playData.During = that._audio.duration;
                that.playData.Data = that._audio.buffered &&
                    that._audio.buffered.length ?
                    (that._audio.buffered.end(0) || 0) :
                    0;
            }, 1000);
            this.playData.IsPlaying = true;
        };
        this._audio.onended = () => {
            window.clearInterval(this.listenInterval);
            this.FillPlayData();
            this.Next('auto');
        };
        this._audio.onabort = () => {
            window.clearInterval(this.listenInterval);
            this.playData.Current = this._audio.currentTime;
            this.playData.Url = this._audio.src;
            this.playData.During = this._audio.duration;
            this.playData.Data = this._audio.buffered &&
                this._audio.buffered.length ?
                (this._audio.buffered.end(0) || 0) :
                0;
            this.playData.IsPlaying = false;
        };
        this._audio.onpause = () => {
            window.clearInterval(this.listenInterval);
            this.playData.Current = this._audio.currentTime;
            this.playData.Url = this._audio.src;
            this.playData.During = this._audio.duration;
            this.playData.Data = this._audio.buffered &&
                this._audio.buffered.length ?
                (this._audio.buffered.end(0) || 0) :
                0;
            this.playData.IsPlaying = false;
        };
        this.playData = { Style: 0, Index: 0 };
        this.playList = [];
    }
    /**
     * 1.列表中无此音频则添加并播放
     * 2.列表中存在此音频但未播放则播放
     * 3.列表中存在此音频且在播放则暂停
     * @param audio
     */
    public Toggle(audio?: Audio): void {
        let tryGet = audio ?
            this.playList.findIndex((p) => p.Url === audio.Url) :
            this.playData.Index;
        if (tryGet < 0) {
            this.playList.push(audio);
            this.PlayIndex(this.playList.length);
        } else {
            if (tryGet === this.playData.Index) {
                if (this._audio.paused) {
                    this._audio.play();
                    this.playData.IsPlaying = true;
                } else {
                    this._audio.pause();
                    this.playData.IsPlaying = false;
                }
            } else {
                this.PlayIndex(tryGet);
            }
        }
    }

    /**
     * 添加单首歌曲到列表
     * 若列表中无此音频则添加到列表的最后
     * 若列表中无音频则添加后并播放
     * @param audio
     */
    public Add(audio: Audio): void {
        this.playList.push(audio);
        if (this.playList.length === 1) {
            this.PlayIndex(0);
        }
    }
    /**
     * 通过传入数组添加歌曲
     * 若列表中无音频则添加后并播放
     * @param audio
     */
    public AddArray(arr): void{
        this.playList = arr.map((item)=>{
            return item;
        })
        if (this.playList.length > 0) {
            this.PlayIndex(0);
        }
    }
    /**
     * 移除列表中指定索引的音频
     * 若移除的就是正在播放的音频则自动播放新的同索引音频，不存在此索引则递减
     * 若只剩这一条音频了则停止播放并移除
     * @param index
     */
    public Remove(index: number): void {
        this.playList.splice(index, 1);
        if (!this.playList.length) {
            this._audio.src = '';
        } else {
            this.PlayIndex(index);
        }
    }

    // /**
    //  * 下一曲
    // type: auto(自动切换下一首) | man（手动切换下一首）-->用来控制循环播放的切换下一首歌
    //  */
    public Next(type): void {
        switch (this.playData.Style) {
            case 0: // 循环播放
                if(this.playData.Index == this.playList.length - 1){
                    this.playData.Index = 0;
                    this.PlayIndex(this.playData.Index);
                }else if(this.playData.Index < this.playList.length){
                    this.playData.Index++;
                    this.PlayIndex(this.playData.Index);
                }
                break;
            case 1: // 随机播放
                const preIndex = this.playData.Index;
                this.playData.Index = Math.ceil((Math.random() * this.playList.length));
                if (preIndex == this.playData.Index){
                    this.playData.Index = this.playData.Index - 1;
                }
                this.PlayIndex(this.playData.Index);
                break;
            case 2: // 单曲循环
                this._audio.currentTime = 0;
                if(type != 'auto'){
                    this.playData.Index = this.playData.Index + 1
                    if(this.playData.Index == this.playList.length){
                        this.playData.Index = 0
                    }
                    this.PlayIndex(this.playData.Index);
                }else{
                    this.PlayIndex(this.playData.Index);
                }
                break;
            default:
                if (this.playData.Index < this.playList.length) {
                    this.playData.Index++;
                    this.PlayIndex(this.playData.Index);
                }
                break;
        }
    }

    /**
     * 上一曲
     * // type: man（手动切换上一首）-->用来控制循环播放的切换上一首歌
     */
    public Prev(type): void {
        switch (this.playData.Style) {
            case 0:
                if(this.playData.Index == 0){
                    this.playData.Index = this.playList.length - 1;
                    this.PlayIndex(this.playData.Index);
                }else if(this.playData.Index > 0){
                    this.playData.Index--;
                    this.PlayIndex(this.playData.Index);
                }
                break;
            case 1:
                const preIndex = this.playData.Index;
                this.playData.Index = Math.floor(Math.random() * this.playList.length);
                if (preIndex == this.playData.Index){
                    this.playData.Index = this.playData.Index - 1;
                }
                this.PlayIndex(this.playData.Index);
                break;
            case 2:
                this._audio.currentTime = 0;
                if(type == 'man'){
                    this.playData.Index = this.playData.Index - 1;
                    if(this.playData.Index < 0){
                        this.playData.Index = this.playList.length - 1;
                        this.PlayIndex(this.playData.Index);
                    }else{
                        this.PlayIndex(this.playData.Index);
                    }
                }
                break;
            default:
                if (this.playData.Index > 0) {
                    this.playData.Index--;
                    this.PlayIndex(this.playData.Index);
                }
                break;
        }
    }

    /**
     * 将当前音频跳转到指定百分比进度处
     * @param percent
     */
    public Skip(percent: number): void {
        this._audio.currentTime = this._audio.duration * percent;
        this.playData.Current = this._audio.currentTime;
    }

    public PlayList(): Audio[] {
        return this.playList;
    }

    public PlayData(): PlayData {
        return this.playData;
    }

    /**
     * 用于播放最后强行填满进度条
     * 防止播放进度偏差导致的用户体验
     */
    private FillPlayData(): void {
        this.playData.Current = this._audio.duration;
        this.playData.Data = this._audio.duration;
    }

    /**
     * 尝试播放指定索引的音频
     * 索引不存在则尝试递增播放，又失败则递减播放，又失败则失败
     * @param index
     */
    private PlayIndex(index: number): void {
        index = this.playList[index] ? index :
            this.playList[index + 1] ? (index + 1) :
                this.playList[index - 1] ? (index - 1) : -1;
        if (index !== -1) {
            this._audio.src = this.playList[index].Url;
            if (this._audio.paused) {
                this._audio.play();
                this.playData.IsPlaying = true;
            }
            this.playData.Index = index;
        } else {
            console.log('nothing to be play');
        }
    }
    // 改变播放模式
    public ChangeMode(mode: number):void{
        this.playData.Style = mode;
        console.log('ModeChange')
    }
}
