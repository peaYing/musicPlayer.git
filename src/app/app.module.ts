import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AudioStudioComponent } from './audio/studio/audio-studio.component';
import { TimePipe } from 'src/pipe/time.pipe';
import { AudioService } from './audio/audio.service';

@NgModule({
  declarations: [
    AppComponent,
    AudioStudioComponent,
    TimePipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [AudioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
